#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const cors = require('cors')
const express = require('express')
const host = '127.0.0.1'
const port = '8080'
const save_path = path.join(path.dirname(require.main.filename), 'notes.json')
const app = express()

function logger(req, res, next) {
    console.log(`${req.ip} ${req.method} ${req.originalUrl}`)
    next()
}

app.use(express.json())
app.use(cors())
app.use(express.static(__dirname))
app.use(logger)

app.get('/notes', (req, res) => {
    fs.readFile(save_path, (err, data) => {
        if (err) {
            res.status(500).send(err)
        } else {
            res.send(JSON.parse(data))
        }
    })
})

app.put('/notes', (req, res) => {
    fs.writeFile(save_path, JSON.stringify(req.body), (err) => {
        if (err) {
            res.status(500).send(err)
        } else {
            res.json({ status: 'success' })
        }
    })
})

app.listen(port, host, () => console.log(`Listening on http://${host}:${port}/`))
