let groups = [
    {
        name: 'Example',
        items: [
            {
                title: 'Hello',
                content: '# Hello, world!'
            }
        ]
    }
]
let selected = [0, 0]
const remoteUri = 'http://127.0.0.1:8080/notes'

const renderer = {
    image(href, title, text) {
        if (!text) text = ''
        if (!title) title = ''
        return `<img class="img-fluid rounded d-flex mx-auto" src="${href}" alt="${text}" title="${title}">`
    },
    code(text) {
        return `
        <pre class="bg-light rounded p-1"><code>${text}</code></pre>
        `
    },
    table(header, body) {
        return `
    <table class="table table-striped">
        <thead>${header}</thead>
        <tbody>${body}</tbody>
    </table>
    `
    }
}

marked.use({ renderer })

async function loadRemote() {
    const response = await fetch(remoteUri)
    if (response.status === 200) {
        groups = await response.json()
        updateGroups()
        selectGroup()
    } else {
        console.log(await response.json())
    }

}

async function saveRemote() {
    const response = await fetch(remoteUri, {
        method: 'PUT',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(groups)
    }).then(r => r.json())
    console.log(response)
}

function shortcuts(event) {
    if (event.ctrlKey === true && event.keyCode === 79) {
        event.preventDefault()
        loadRemote()
    } else if (event.ctrlKey === true && event.keyCode === 83) {
        event.preventDefault()
        saveRemote()
    }
}

function updateGroups() {
    groupsSelect.innerHTML = ''
    if (groups.length > 0) {
        groups.forEach(group => {
            groupsSelect.innerHTML += `<option>${group.name}</option>`
        })
        groupsSelect.selectedIndex = selected[0]
        selectGroup()
    } else {
        itemsSelect.innerHTML = ''
        markdownInput.value = ''
        markdownOutput.innerHTML = ''
        addGroup(false)
    }
}

function updateItems() {
    itemsSelect.innerHTML = ''
    if (groups[selected[0]].items.length > 0) {
        groups[selected[0]].items.forEach(item => {
            itemsSelect.innerHTML += `<option>${item.title}</option>`
        })
        itemsSelect.selectedIndex = selected[1]
        selectItem()
    } else {
        markdownInput.value = ''
        markdownOutput.innerHTML = ''
        addItem(false)
    }
}

function selectGroup() {
    selected[0] = groupsSelect.selectedIndex
    updateItems()
}

function selectItem() {
    selected[1] = itemsSelect.selectedIndex
    document.title = `${groups[selected[0]].name} - ${groups[selected[0]].items[selected[1]].title}`
    markdownInput.value = groups[selected[0]].items[selected[1]].content
    markdownInput.focus()
    parseMarkdown()
}

function addGroup(event) {
    if (event === true || event.keyCode === 13) {
        groupsSelect.parentElement.classList.remove('d-none')
        groupsInput.parentElement.classList.add('d-none')
        groups.push({
            name: groupsInput.value,
            items: []
        })
        groupsInput.value = ''
        selected[0] = groups.length - 1
        updateGroups()
        addItem(false)
    } else {
        groupsSelect.parentElement.classList.add('d-none')
        groupsInput.parentElement.classList.remove('d-none')
        groupsInput.focus()
    }
}

function addItem(event) {
    if (event === true || event.keyCode === 13) {
        itemsSelect.parentElement.classList.remove('d-none')
        itemsInput.parentElement.classList.add('d-none')
        groups[selected[0]].items.push({
            title: itemsInput.value,
            content: ''
        })
        itemsInput.value = ''
        selected[1] = groups[selected[0]].items.length - 1
        updateItems()
    } else {
        itemsSelect.parentElement.classList.add('d-none')
        itemsInput.parentElement.classList.remove('d-none')
        itemsInput.focus()
    }
}

function removeGroup() {
    groups.splice(selected[0], 1)
    selected[0] = 0
    updateGroups()
}

function removeItem() {
    groups[selected[0]].items.splice(selected[1], 1)
    selected[1] = 0
    updateItems()
}

function parseMarkdown() {
    if (groups.length > 0 && groups[selected[0]].items[selected[1]]) {
        groups[selected[0]].items[selected[1]].content = markdownInput.value
        markdownOutput.innerHTML = marked.parse(markdownInput.value)
    }
}

function isTab(event) {
    if (event.keyCode === 9) {
        markdownInput.setRangeText(
            '\t',
            markdownInput.selectionStart,
            markdownInput.selectionStart,
            'end'
        )
        event.preventDefault()
    }
    parseMarkdown()
}

loadRemote()
updateGroups()
